resource "vsphere_virtual_machine" "ansible-towerhub" {
  name             = "ansiblehub-cpd4-lamnd"
  datastore_id     = data.vsphere_datastore.datastore_ssd1.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  num_cpus         = 4
  memory           = 4096
  firmware = "efi"

  network_interface {
    network_id   = data.vsphere_network.vm_network.id 
    adapter_type = "vmxnet3"
  }

  disk {
    label            = "disk0-bastion.ocp4"
    size             = data.vsphere_virtual_machine.template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }


  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
   

    customize {
      linux_options {
        host_name = "ansible-hub"
        domain    = "ocp4.svtech.gay"
      }

      network_interface {
        ipv4_address = "10.1.30.23"
        ipv4_netmask = 16
      }
      
     
      ipv4_gateway = "10.1.0.1"
      dns_server_list = var.dns_servers
    }
  }

}