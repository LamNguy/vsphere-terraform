resource "vsphere_virtual_machine" "worker01" {
  name             = "worker01-k8s-ducnv"
  datastore_id     = data.vsphere_datastore.datastore_ssd2.id
  host_system_id   = data.vsphere_host.host_02.id
  resource_pool_id = data.vsphere_resource_pool.pool_02.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  num_cpus         = 8
  memory           = 8192
  firmware = "efi"


  network_interface {
    network_id   = data.vsphere_network.vlan555_network_02.id
    adapter_type = "vmxnet3"
  }

  disk {
    label            = "disk01-worker01"
    size             = data.vsphere_virtual_machine.template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }


  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
   

    customize {
      linux_options {
        host_name = "worker01-k8s"
        domain    = "k8s.svtech.gay"
      }
      
      network_interface {
        ipv4_address = "192.168.200.53"
        ipv4_netmask = 24 
      }
   
      dns_server_list = var.dns_servers
    }
  }

}