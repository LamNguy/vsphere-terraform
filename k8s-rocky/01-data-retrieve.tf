# Define datacenter
data "vsphere_datacenter" "dc" {
  name = var.dc
}

data "vsphere_host" "host_03" {
  name          = var.host_03
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_host" "host_02" {
  name          = var.host_02
  datacenter_id = data.vsphere_datacenter.dc.id
}


data "vsphere_resource_pool" "pool_03" {
  name          = "khanh.chu__esx-03"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool_02" {
  name          = "khanh.chu__esx-02"
  datacenter_id = data.vsphere_datacenter.dc.id
}



data "vsphere_datastore" "datastore_ssd3" {
  name          = "esx-03_cloud_ssd01"
  datacenter_id = data.vsphere_datacenter.dc.id
}


data "vsphere_datastore" "datastore_ssd2" {
  name          = "10.1.0.21_ssd03_nvme"
  datacenter_id = data.vsphere_datacenter.dc.id
}


data "vsphere_network" "vlan555_network_03" {
  name          = "DPortGroup-vlan1710-tungdoan"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "vlan555_network_02" {
  name          = "DPortGroup-vlan1710-tungdoan"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.template_image
  datacenter_id = data.vsphere_datacenter.dc.id
}
