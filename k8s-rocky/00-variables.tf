provider "vsphere" {
  user                 = "<username>"
  password             = "<password"
  vsphere_server       = "<ip_vcenter>"
  allow_unverified_ssl = true
}

# define datacenter
variable "dc" {
  default = "svtechhn"
}

# define cluster
variable "cluster" {
  default = "na"
}

# define host
variable "host_03" {
  default = "esx-03"
}

# define host
variable "host_02" {
  default = "esx-02"
}


# define dns server
variable "dns_servers" {
  default = ["8.8.8.8"]
}

# define vm template
variable "template_image" {
  default = "rocky8-template"
}
