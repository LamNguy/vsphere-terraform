resource "vsphere_virtual_machine" "registry" {
  name             = "registry-cpd4-lamnd"
  datastore_id     = data.vsphere_datastore.datastore_ssd2.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  num_cpus         = 4
  memory           = 8192
  firmware = "efi"

  network_interface {
    network_id   = data.vsphere_network.vm_network.id 
    adapter_type = "vmxnet3"
  }

  network_interface {
    network_id   = data.vsphere_network.cpd_network.id
    adapter_type = "vmxnet3"
  }

  disk {
    label            = "disk0-registry-ocp4"
    size             = data.vsphere_virtual_machine.template.disks.0.size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  disk {
    label             = "disk1-registry-ocp4"
    size             = 500
    unit_number      = 1
    thin_provisioned = true
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
   

    customize {
      linux_options {
        host_name = "registry"
        domain    = "ocp4.svtech.gay"
      }

      network_interface {
        ipv4_address = "10.1.30.18"
        ipv4_netmask = 16
      }
      
      network_interface {
        ipv4_address = "192.168.30.18"
        ipv4_netmask = 24 
      }
   
      ipv4_gateway = "10.1.0.1"
      dns_server_list = var.dns_servers
    }
  }

}