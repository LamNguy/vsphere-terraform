# Define datacenter
data "vsphere_datacenter" "dc" {
  name = var.dc
}

# Exctrat data port vlan creation
data "vsphere_host" "host" {
  name          = var.host
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = "terraform"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_ssd2" {
  name          = "ssd03_nvme_21"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_ssd1" {
  name          = "ssd01_21"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_ssd3" {
  name          = "ssd02_21"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore_hdd1" {
  name          = "10.1.0.21-hdd1"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "cpd_network" {
  name          = "cpd4"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "vm_network" {
  name          = "VM Network"
  datacenter_id = data.vsphere_datacenter.dc.id
}


data "vsphere_virtual_machine" "template" {
  name          = var.template_image
  datacenter_id = data.vsphere_datacenter.dc.id
}


