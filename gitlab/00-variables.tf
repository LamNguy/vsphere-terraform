provider "vsphere" {
  user           = "administrator@vsphere.local"
  password       = "Changeme@123"
  vsphere_server = "10.1.0.23"
  allow_unverified_ssl = true
}

# define vm template
variable "template_image" {
  default = "rhel-8.6-template"
}

# define datacenter
variable "dc" {
  default = "SVTECH_HN"
}

# define cluster
variable "cluster" {
  default = "na"
}

# define host
variable "host" {
  default = "10.1.0.21"
}

# define dns server
variable "dns_servers" {
  default = ["8.8.8.8"]
}


