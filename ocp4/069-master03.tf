resource "vsphere_virtual_machine" "master03" {
  name             = "master03-cpd4-lamnd"
  datastore_id     = data.vsphere_datastore.datastore_ssd2.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = "otherGuest64"
  num_cpus         = 8
  memory           = 32768
  firmware = "efi"
  hardware_version = 14
  
  
  # cdrom {
  #   datastore_id = data.vsphere_datastore.datastore_ssd1.id   # Specify the ID or name of the datastore
  #   path        = "ISO/rhcos-4.11.9-x86_64-live.x86_64.iso"  # Specify the path to the ISO image
  # }

  network_interface {
    network_id   = data.vsphere_network.cpd_network.id
    adapter_type = "vmxnet3"
    use_static_mac = true
    mac_address = "00:50:56:11:6B:9C"
  }

  disk {
    label            = "disk0-master03.ocp4"
    size             = 100
    eagerly_scrub    = false
    thin_provisioned = true
  }


}